# ML4HC

Repository for the ML4HC projects of the course [*Machine Learning for Health Care*](https://bmi.inf.ethz.ch/teaching/261-5120-00l-machine-learning-for-health-care-spring-2021) 

The main branch will contain only working code that can be submitted. Commit to separate branches when trying things out.

Link to the Overleaf for project 1: [*Report*](01-ECG_Classification/ML4HC_Project1.pdf) 

Link to the Overleaf for project 2: [*Report*](02-Natural_Language_Processing/ML4HC_Project2.pdf) 

Link to the Overleaf for project 3: [*Report*](03-Medical_Imaging/ML4HC_Project3.pdf) 
