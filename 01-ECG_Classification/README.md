# ML4HC
## ECG Classification Task
In this project, we try to classify ECGs. The datasets used are PTBDB, where there are two classes of ECGs to identify, and MITBIH, where there are 5 classes.

## Remarks

The given requirements.txt contains all the python packages that are needed to successfully run the notebooks. To run the notebooks, simply go through and manually run the cells that should be run or press run-all (warning: this may lead to models and tuners being run and those can take a long time).

The data was normalized with the z-score except in the STFT model and in the second ensemble method.

### For all non Deep Metric learning models one can predict the y_pred as always

For example, to use the CNN_mitbih model, we have to define the path to the folder

- model_path = ".../03-Models/02-Mitbih/01-Task1/CNN_mitbih"
- model = tf.keras.models.load_model(model_path)

**Ptbdb-predictions**

- pred_test = model.predict(X_test)
- pred_test = (pred_test>0.5).astype(np.int8)

**Mitbih-predictions**

- pred_test = model.predict(X_test)
- pred_test = np.argmax(pred_test, axis=-1)

### For all Deep Metric Learning models a different appraoch is needed
Because we determine the class of the query data via ANN, we use the build in tensorflow similarity function matching.

**reload the model**
- reloaded_model = tf.keras.models.load_model(
    save_path,
    custom_objects={"SimilarityModel": tfsim.models.SimilarityModel},
)

**reload the index**

- reloaded_model.load_index(save_path)

**check the index is back**

- reloaded_model.index_summary()

**Predictions (matches is equivalent to pred_test)**

- cutpoint = "optimal"
- matches = model.match(X_test, cutpoint=cutpoint, no_match_label=0)

Note, some queries will be further away from indexed samples than the optimal cutoff distance, meaning they will have no class, this is a small minority, but it happens. We set no_match_label=0 such that they always have a class.


### Tensorboard visualizations
Tensorboard needs to be installed.
We look at the example of the ptbdb logs. The procedure is transferable to the mibih logs.

**In your console use the command to see the results of all runs**

- tensorboard --logdir='...\04-Logs\01-Ptbdb'

**to see results only of ptbdb task1 logs**

- tensorboard --logdir='...\04-Logs\01-Ptbdb\01-Task1'

**to see results only of ptbdb task1 CNN logs**

- tensorboard --logdir='...\04-Logs\01-Ptbdb\01-Task1\CNN_ptbdb'

Notes:
- to see the projections you have to refresh the log at the top right or select projector from the menu.
- Plots such as Confusion matrix, AUROC are displayed by epochs in the images tab, use the dot to move from epochs.
- The Projections are labeled choose labels in the projector tab on the left top to see the distinct clusters
- Change from train to validation in the Projector to see the plots of the time series data viusallized in the embedding space insted of the dots
- Projections are only visible when at least one deep metric leanring approach is choosen
- Using model.predict() on the dml models will output the embedding vectors which can be used for what ever
