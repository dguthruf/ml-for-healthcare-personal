# ML4HC
## NLP Classification Task
In this project, we try to classify texts from abtracts of medical papers.

## Remarks

To run our code, set up the conda environment with the .txt file provided. Then all the notebooks should be able to run with no problem. Python version used: 3.9, 3.10. 
The code for the individual models/tasks can be found under 02-Code. Some of the models may take a long time to run, so proceed with caution. 

Some models were saved and can be found and loaded from 03-Models. 
